#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>


unsigned int CalculateNumbers(std::vector<unsigned int>vector, unsigned int noun, unsigned int verb)
{
    if (vector.size() >=4)
    {
        vector.at(1) = noun;
        vector.at(2) = verb;
        for (std::size_t i{0}; i < vector.size(); i += 4)
        {
            if (vector.at(i) == 1)
            {
                vector.at(vector.at(i + 3)) = vector.at(vector.at(i + 1)) + vector.at(vector.at(i + 2));
            }
            else if (vector.at(i) == 2)
            {
                vector.at(vector.at(i + 3)) = vector.at(vector.at(i + 1)) * vector.at(vector.at(i + 2));
            }
            else if (vector.at(i) == 99)
            {
                break;
            }
        }
    }
    const auto result = vector.empty() ? 0U : vector.at(0);
    return result;
}

int main()
{
    std::string inputFilename{"/home/qxz0abk/advent_of_code/untitled/input_4.txt"};
    try {
        std::ifstream inputStream(inputFilename);
        std::vector<unsigned int> numbers;
        unsigned int number{0U};
        while ((inputStream >> number) && inputStream.ignore())
        {
            numbers.push_back(number);
        }
        if (!numbers.empty())
        {
            for (std::size_t i{0}; i <= 99; ++i)
            {
                for (std::size_t j{0}; j <= 99; ++j)
                {
                    const auto desired_result{19690720U};
                    if (CalculateNumbers(numbers, i, j) == desired_result)
                        std::cout << i << " " << j << std::endl;
                }
            }
        }
    }
    catch (const std::ifstream::failure &e)
    {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    return 0;
}

